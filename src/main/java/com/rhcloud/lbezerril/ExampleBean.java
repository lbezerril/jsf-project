package com.rhcloud.lbezerril;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class ExampleBean {
	
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
